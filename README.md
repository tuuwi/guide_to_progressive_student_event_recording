# \"Wo muss ich da jetzt klicken?\" - Ein Leitfaden zur progressiven studentischen Veranstaltungsaufzeichnung mit Theorie und Praxis

## Mit Inputs zur studentischen Selbstorganisation, freier Software, OER und praktischer Umsetzung.

Dieses Repo geht aus dem Ergebnis unseres einjährigen Projekts zur Digitalisierung der Umweltringvorlesungen (URV) im Rahmen des Digital-Fellowship-Programms hervor. Herausgekommen ist ein Leitfaden, der unsere Erfahrungen, Tipps und Tricks zur studentischen Veranstaltungsaufzeichnung, die wir während des Projekts gesammelt haben, zusammenfasst. Ehemals als reiner Leitfaden zur Veranstaltungsaufzeichnung in Präsenz gedacht, hat er durch den Einfluss von Corona eine starke Prägung in Richtung Online-Lehre erfahren. Doch nicht nur das, aufgrund verschiedener Schwierigkeiten, ein solches Projekt mit Mitteln des Sächsischen Freistaates in studentischer Eigenregie an unserer Uni umzusetzen, ist zudem eine Streitschrift für studentische Selbverwaltung entstanden. Weiterhin spielen in unserem Projekt Freies Wissen und Freie Software eine entscheidene Rolle. Das Repo ist der Ort, um gemeinsam den Leitfaden weiter zu entwickeln und zu updaten. Mehr Infos dazu unter [Mithelfen und Verbessern](#mithelfen-und-verbessern)

## Mithelfen und Verbessern
Wir möchten nicht, dass unser Leitfaden einfach nur den Istzustand zum Zeitpunkt unseres Projekts darstellt und dann als PDF irgendwo auf der Festplatte verstaubt. So wie sich unserere Sichtweise und unser Wissenstand ändert und erweitert, so ändern sich auch die Prozesse/Programme, die wir im Leitfaden beschrieben haben.
Aus diesem Grund möchten wir gerne allen die Möglichkeiten geben sich an der Aktualisierung und Erweiterung des Leitfadens zu beteiligen. Dadurch kann der Leitfaden von einer reinen auf unsere Sichtweise beschränkten Momentaufnahme, zu einem offenen und sich ständig weiterentwickelnden Wissenssammlung und Erfahrungsbericht für alle werden.

Um dir und uns die Möglichkeit zu geben, den Text strukturiert bearbeiten zu können und eine klare Versionierung sicher zu stellen, haben wir für den Leitfaden dieses Repo erstellt. 

In Zukunft soll es möglich sein über Pullrequests Änderungen bzw. Ergänzungen vorzuschlagen, die wir dann einpflegen.

## Anmerkung zu den verschiedenen Versionen dieses Leitfadens
Es gibt zwei Versionen dieses Leitfadens:
1. Die [einmalig gelayoutete Broschüre](https://tuuwi.de/wp-content/uploads/Wo-muss-ich-da-jetzt-klicken.pdf), die den Istzustand von März 2021 darstellt und damit den Abschluss unsereres eigentlichen Projekts markiert. Diese Version wurde mit einer Layoutingsoftware erstellt, wird nicht geupdatet werden und ist damit relativ losgelöst von diesem Repo ist. 
2. Die ständig aktualisierte Version, die sich gerade hier im Repo im Aufbau befindet und die wie oben beschrieben allen offen zur Bearbeitung steht. Geplant ist es immer wieder, wenn genügend Änderungen eingereicht wurden, eine neue Version z.B. als Webseite oder PDF-Datei zu erstellen bzw. allen die selbstständige Genererierung einer solchen handlichen Version zu ermöglichen. (Siehe [Issue #3](https://codeberg.org/tuuwi/guide_to_progressive_student_event_recording/issues/3))

## Using pandoc for tranforming the raw markdown-files to a pdf
1. Install [Pandoc](https://pandoc.org/installing.html) and [LaTeX](https://en.wikibooks.org/wiki/LaTeX/Installation#Distributions) on your System.
    - this repo was tested with `TeX Live` on ArchLinux
2. Download the [modified Eisvogel LaTeX template](https://github.com/alexeygumirov/pandoc-for-pdf-how-to/blob/master/pandoc/templates/eisvogel_mod.latex). This one is used to give the generated pdf a suitable layout. Of course you could also experiment with your own templates.
3. Move the template  `eisvogel_mod.latex` to your pandoc template folder. The location depends on your operating system and should be in the default user data directory which you can look up with `pandoc --version`.
    - on Unix, Linux, macOS the template folder should be in `~/.local/share/pandoc/templates/` or `~/.local/share/pandoc/templates/`
    If there are no folders called `templates` or `pandoc` you need to create them and put the template `eisvogel_mod.latex` inside.

Now you have all the tools for builing the pdf ready. 

4. Clone this repo with `git clone https://codeberg.org/tuuwi/guide_to_progressive_student_event_recording.git`
5. Change dir with `cd guide_to_progressive_student_event_recording`
6. Execute the building shell script with `./make_pdf.sh`
7. The script outputs a pdf file named `Wo-muss-ich-da-jetzt-klicken-VX.pdf` in your current folder where X is the current version.

### Problems during building process
- build script prints error: `Could not find data file /usr/share/pandoc/data/templates/eisvogel_mod.latex`
    - Pandoc is looking for the template in another folder. Just put the template their and it should work.

## Formatierungshinweise
- Zur einfachen Bearbeitung des Leitfadens nutzen wir die Auszeichnungssprache [Markdown](https://de.wikipedia.org/wiki/Markdown). Die Markdown-Syntax könnt ihr z.B. in folgender Anleitung nachsehen: [Basic Syntax](https://www.markdownguide.org/basic-syntax)
- Für die Generierung der finalen PDF mit pandoc, wie in der obigen Anleitung beschrieben, nutzen wir außerdem noch einige spezielle Formatierungen:
    - Fußnoten werden mit der pandoceigenen Markdown-Syntax `^[Fußnotentext]` direkt an die entsprechende Stelle im Text eingefügt. Pandoc generiert daraus automatisch Fußnoten mit entsprechender fortlaufender Nummerierung
    - damit Kapitelüberschriften ordentlich erkannt werden, muss am Anfang jeder verarbeiteten Kapiteldatei eine Leerzeile stehen
    - außerdem ist es möglich Latexsyntax direkt in den Markdown-Dokumenten zu verwenden:   
        - `\newpage{}` sorgt z.B. für einen Seitenumbruch in der generierten PDF

## Inhaltsverzeichnis

1.  [**Vorwort**](./chapters/01_Vorwort.md)

    1.  Wer sind wir? Was wollen wir? Oh man, was passiert?

    2.  Interview zwischen DF und tuuwi

2.  [**THEORIE: Streitschrift für freies Wissen, freie Software und
    studentische Selbstorganisation**](./chapters/02_Theorie.md)

    1.  Studentische Selbstverwaltung - (fehlende) Grundlagen des
        Arbeitens

    2.  freies Wissen - OER

        1.   Lizenzierung - Creative Commons

        2.  Schwierigkeiten

    3.  freie Software - Open-Source-Programme

3.  [**PRAXIS: Leitfaden zur studentischen Veranstaltungsaufzeichnung**](./chapters/03_Praxis.md)

    1.  Einleitung

    2.  Kommunikation mit Referierenden

    3.  Aufnahme der Veranstaltung

    4.  Bearbeitung des Materials

    5.  Veröffentlichung des Materials

4.  [**Fazit und Lessons Learned**](./chapters/04_Fazit_und_Lessons_Learned.md)

    1.  Zusammenfassung

    2.  Lessons Learned

    3.  Forderungen

5.  **Anhang**

    1.  [Checklisten zur Veranstaltungsaufzeichnung](./chapters/05_Anhang/Checklisten_zur_Veranstaltungsaufzeichnung.md)

    2.  [Programmübersicht](./chapters/05_Anhang/Programmuebersicht.md)

    3.  Vorlagen 

        1.  [Einverständniserklärungen](./chapters/05_Anhang/5_3_1_Einverstaendniserklaerungen.md)

        2.  [selbstständigen Aufnahme einer Veranstaltung mit OBS und OER](./chapters/05_Anhang/5_3_2_Leitfaden_OBS_OER.md)

## Impressum
2021 TU-Umweltinitiative (tuuwi)

![tuuwi-logo](./images/logos/tuuwi.png)

**Herausgeberin**: TU-Umweltinitiative (tuuwi) der TU Dresden\
**Autor:innen**: Kai Witza, Cornell Ziepel, Jennifer Vaupel\
**Umschlaggestaltung, Illustration, Layout**: Hannah Röbisch\
**weitere Mitwirkende**: Jessica Flecks, Joshua Größer, Undine Krätzig (als Vertretung des Dezernats 8 der TU
Dresden), Toni Mudrak, Kristian Prewitz,
Jana Riedel (als Vertretung für das Digital-
Fellowship-Programm), Tom Stieler\
\
Das Werk, einschließlich seiner Teile, ist unter der Creative Commons
Lizenz CC BY- SA 4.0 veröffentlicht 

![CC BY SA](./images/logos/cc-by-sa.png)    

D.h. ihr dürft das Material in jedwedem Format oder Medium
vervielfältigen und weiterverbreiten sowie remixen, verändern und darauf
aufbauen - alles definitiv wünschenswert!\
Folgende Bedingungen solltet ihr aber dabei beachten:

-   Namensnennung

-   Weitergabe unter gleichen Bedingungen

Das Werk ist im Rahmen des Digital-Fellowship-Programms im
Förderzeitraum 2019/2020 des Hochschuldidaktischen Zentrums Sachsen
(HDS) und des Arbeitskreis E-Learning der LRK Sachsen (AK E-Learning)
entstanden.

Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des von den Abgeordneten des Sächsischen Landtages beschlossenen Haushaltes.
<br></br>