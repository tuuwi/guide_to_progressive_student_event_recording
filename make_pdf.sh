#!/bin/sh

# This script is for the local generation of the PDF file
# You shall install pandoc and texlive packages to make it work

VERSION=1-0-1
DEST_FILE_NAME="Wo_muss_ich_da_jetzt_klicken-v$VERSION.pdf"
INDEX_FILE="assets/INDEX"
TEMPLATE="eisvogel_mod"
DATE=$(date "+%B %Y")

pandoc -s -o "$DEST_FILE_NAME" --template "$TEMPLATE" --toc --columns=50 --dpi=300 --pdf-engine xelatex -M date="$DATE" $(cat "$INDEX_FILE")