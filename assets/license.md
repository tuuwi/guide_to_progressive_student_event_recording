\newpage{}
\vspace*{\fill}
Das Werk, einschließlich seiner Teile, ist unter der Creative Commons
Lizenz CC BY- SA 4.0 veröffentlicht 

![Logo CC BY-SA](images/logos/cc-by-sa.png)    

D.h. ihr dürft das Material in jedwedem Format oder Medium
vervielfältigen und weiterverbreiten sowie remixen, verändern und darauf
aufbauen - alles definitiv wünschenswert!\
Folgende Bedingungen solltet ihr aber dabei beachten:

-   Namensnennung

-   Weitergabe unter gleichen Bedingungen

Das Werk ist im Rahmen des Digital-Fellowship-Programms im
Förderzeitraum 2019/2020 des Hochschuldidaktischen Zentrums Sachsen
(HDS) und des Arbeitskreis E-Learning der LRK Sachsen (AK E-Learning)
entstanden.

Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des von den Abgeordneten des Sächsischen Landtages beschlossenen Haushaltes.