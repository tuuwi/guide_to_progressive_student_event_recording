
## 5.3.2  Leitfaden zur selbstständigen Aufnahme eines Beitrages einer URV

Um trotz der aktuellen Situation die Umweltringvorlesung (URV) (in
eingeschränkten Maßen) vorführen zu können, möchten wir die Vorträge der
URV gerne als Videos für die Studierenden und am besten alle
Interessierten zum Download anbieten. Wie Sie ihren Vortrag aufnehmen
und uns zukommen lassen, erklären wir Ihnen in der folgenden Anleitung.
Ebenfalls werden wir im zweiten Teil auf das Thema Lizenzierung und
*Open Educational Ressources* eingehen. Bitte denken Sie daran
rechtzeitig Ihren Vortrag aufzunehmen, um bei eventuell aufkommenden
Problemen, noch genügend Puffer zu haben. Vielen Dank, dass Sie sich die
Zeit nehmen und so zu einer digitalen URV beitragen.

### Inhaltsverzeichnis

- [Aufnahme der Vorlesung mit OBS](#aufnahme-der-vorlesung-mit-obs)

    - [Installation](#installation)

    - [Einrichtung](#einrichtung)

        - [Autokonfiguration](#autokonfiguration)

        - [Speicherort und Speicherformat](#speicherort-und-speicherformat)

        - [Szeneneinrichtung](#szeneneinrichtung)

        - [Audiokonfiguration](#audiokonfiguration)

        - [Beispielhafte Einrichtung einer Aufnahme](#beispielhafte-einrichtung-einer-aufnahme)

    - [Aufnahme](#aufnahme)

    - [Versenden](#versenden)

- [Lizensierung -- Open Educational Resources (OER)](#lizensierung-open-educational-resources-oer)

### Aufnahme der Vorlesung mit OBS

Zum Aufnehmen empfehlen wir OBS (Open Broadcaster Software). Dies ist
ein kostenloses
[OpenSource](https://fsfw-dresden.de/2020/04/DiLi-News-Interview.html)
Programm zur Aufnahme von Videos, welches bereits alle Funktionen
mitbringt, die wir für die gesamte Aufnahme benötigen und für alle
gängigen Betriebssysteme verfügbar ist.

#### Installation

Vorrausetzungen:

-   Laptop/Rechner mit einem Betriebssystem ihrer Wahl (Windows, MacOS
    oder Linux)

-   Mikrofon und Webcam (optional) -- die im Laptop eingebauten
    Aufnahmegeräte sind ausreichend

Laden Sie sich das Installationsprogramm für Ihr Betriebssystem auf [der
offiziellen Seite des OBS Project](https://obsproject.com/) herunter und
folgen Sie den Installationshinweisen.

#### Einrichtung

##### Autokonfiguration

Nachdem Sie OBS erfolgreich installiert haben, starten Sie das Programm
und bestätigen nach dem ersten Start, dass die
Autokonfigurationsassistent ausgeführt werden soll. Anschließend öffnen
sich nacheinander verschiedene Fenster, um einige Grundeinstellungen für
die Verwendung von OBS vorzunehmen. Dieser Assistent lässt sich auch
später über das Menü erneut öffnen.

Folgende Einstellungen sind dabei zu wählen:

Wählen Sie im ersten Schritt „Für das Aufnehmen optimieren" aus, da wir
mit OBS nur aufnehmen möchten und nicht streamen möchten. \[Abb. 5\]

![Autokonfiguration – Informationen zur Verwendung (Screenshot Dialogfenster)](images/Anhang_Leitfaden_OBS_OER/screenshot1.png){width="2.158536745406824in"
height="1.2561701662292213in"}

Im Nächsten Schritt wählen Sie eine geringere Auflösung von 1280x720 und
30 FPS aus, um den Speicherverbrauch des endgültigen Videos geringer zu
halten. \[Abb. 6\]

![Autokonfiguration - Videoeinstellungen (Screenshot Dialogfenster)](images/Anhang_Leitfaden_OBS_OER/screenshot2.png){width="2.577587489063867in"
height="2.072398293963255in"}

Bestätigen Sie weitere Dialogfenster und schließen Sie damit die
Autokonfiguration ab.

##### Speicherort und Speicherformat

Nun sollten Sie den Speicherort einstellen, an dem das fertige Video auf
der Festplatte gespeichert werden soll. Dazu öffnen Sie die
Einstellungen (z.B. über den Button *Einstellungen* rechts unten im
Programm unter der Überschrift Steuerung). \[Abb. 7\]

![Einstellungen öffnen (Screenshot Dialogfenster)](images/Anhang_Leitfaden_OBS_OER/screenshot3.png){width="2.5770833333333334in"
height="1.3465310586176729in"}

Dort lässt sich im Reiter *Ausgabe* der Aufnahmepfad anpassen, an dem
OBS die Aufnahme später speichern soll. Außerdem sollte bei
*Aufnahmeformat* „*mkv*" eingestellt sein. Dies ermöglicht die
Wiederherstellung der Aufnahme, falls der Computer oder das Programm aus
irgendeinem Grund zwischendurch abstürzen sollte. Bestätigen Sie die
Einstellungen mit *Okay*. \[Abb. 8\]

![Ausgabeeinstellungen (Screenshot Programmfenster)](images/Anhang_Leitfaden_OBS_OER/screenshot4.png){width="3.8597561242344707in"
height="3.029562554680665in"}

##### Szeneneinrichtung

Im letzten Schritt der Einrichtung geht es nun darum, die Aufnahme als
solches einzurichten. OBS arbeitet hierbei mit Szenen. In einer Szene
werden alle Quellen (Mikrofon, Kameraaufnahme, Bildschirmaufnahme,
eingeblendetes Bild, etc.) und die Anordnung dieser Quellen in der
fertigen Aufnahme bzw. der Aufbau einer Aufnahme festgelegt. Es handelt
sich dabei quasi um ein Art Drehbuch für das fertige Video. Für unseren
Anwendungsfall haben wir drei mögliche Konfigurationen:

1.  Bildschirmaufnahme mit Ton

2.  Kameraaufnahme mit Ton

3.  Bildschirm- und Kameraaufnahme mit Ton, wobei die Kamera als ein
    kleineres Fenster (Bild in Bild) in einer Ecke des Bildschirms
    angezeigt wird

Alle Einstellungen und eine Übersicht über die Szenen und Quellen
befinden sich im unteren Fensterbereich des Programms. \[Abb. 9\]

![Unterer Programmbereich OBS (Screenshot Programmfenster)](images/Anhang_Leitfaden_OBS_OER/screenshot5.png){width="6.295833333333333in"
height="1.0888888888888888in"}

##### Audiokonfiguration

OBS nimmt standartmäßig bereits Audio auf, dafür wird keine extra
Quelle, wie z.B. für die Bildschirmaufnahme oder die Webcam Aufnahme
benötigt.

Die Audioeinstellungen finden Sie unter *Audio-Mixer* in der unteren
Leiste des Programms. \[Abb. 10\]

![Zahnrad zum Öffnen der Audioeinstellungen (Screenshot Programmfenster)](images/Anhang_Leitfaden_OBS_OER/screenshot5.png){width="6.295833333333333in"
height="1.0888888888888888in"}

Versichern Sie sich zuerst, dass OBS das korrekte Mikrofon verwendet.
Dazu klicken Sie auf das kleine Zahnrad im *Audio-Mixer* neben der
Audiospur mit der Überschrift *Mic/Aux* und wählen in der nun
angezeigten Liste *Eigenschaften* aus. \[Abb. 11\]

![Auswahlliste Audioeinstellungen (Screenshot Programmfenster)](images/Anhang_Leitfaden_OBS_OER/screenshot6.png){width="4.347561242344707in"
height="1.977166447944007in"}

Abbildung : Auswahlliste Audioeinstellungen

Im Fenster, welches sich nun geöffnet hat, können Sie ihr Mikrofon
auswählen, welches für die Aufnahme verwendet werden soll. Dies ist z.B.
ein an den PC angeschlossenes externes Mikrofon oder das intern verbaute
Mikrofon Ihres Laptops. Standard steht hierbei, für das Mikrofon,
welches auch Ihr Betriebssystem gerade verwendet. \[Abb. 12\]

![Auswahl des Mikrofons (Screenshot Progammfenster)](images/Anhang_Leitfaden_OBS_OER/screenshot7.png){width="2.341463254593176in"
height="1.9690398075240594in"}

Im nächsten Schritt sollten Sie die Lautstärke der Aufnahme überprüfen.
Schauen sie dazu wieder in den Bereich *Audio-Mixer* im unteren
Programmabschnitt. Sprechen Sie in Ihrer Vortragslautstärke ein paar
Worte ins Mikrofon. Der Ausschlag des Pegels in der *Mix/Aux* Leiste
sollte im grünen Bereich liegen. Ist dies nicht der Fall, passen Sie die
Lautstärke der Audioaufnahme über den darunter liegenden
Lautstärkeregler an.

##### Beispielhafte Einrichtung einer Aufnahme

In diesem Teil soll beispielhaft eine einfache Version der Aufnahme des
Bildschirms in Kombination mit einer Kameraaufnahme erklärt werden.\
Wenn Sie das ganze etwas professioneller angehen möchten (mit starrem
Tuuwi Hintergrund und angezeigtem Namen) oder eine Videoanleitung
bevorzugen, sei ihnen dieses Video von Prof. Dr. Lasch empfohlen:\
[Digitale Lehre 3: Vidcasting](https://youtu.be/SpRZZBmPwOI?t=381)

Den Videohintergrund im Tuuwi Corporate Design finden Sie ebenfalls im
Cloudstoreordner, den wir mit Ihnen geteilt haben.

Wir freuen uns natürlich über ein professionelles Aussehen der
endgültigen Videos, verstehen aber auch, wenn Sie eine einfachere
Einrichtung bevorzugen.

Wie oben beschrieben nimmt OBS bereits standartmäßig den Ton auf. Da wir
nur eine Tonspur aufnehmen möchten, brauchen Sie diese also nicht extra
unter Quellen hinzufügen.

Aus diesem Grund fügen Sie im nächsten Schritt als erste Quelle die
*Bildschirmaufnahme* hinzu.

Klicken Sie dazu unter *Quellen* auf das Plus und wählen sie in der
Auswahlliste *Bildschirmaufnahme*. \[Abb. 13\]

![Neue Quelle „Bildschirmaufnahme“ hinzufügen (Screenshot Programmfenster)](images/Anhang_Leitfaden_OBS_OER/screenshot8.png){width="3.6219510061242346in"
height="2.69588801399825in"}

Im daraufhin erscheinenden Fenster können Sie einen Titel für die Quelle
festlegen. Dieser Name wird nur in der Quellenübersicht angezeigt, dient
also nur der Übersicht. Bestätigen Sie das Fenster mit *Okay.* \[Abb.
14\]

![Quelle benennen](images/Anhang_Leitfaden_OBS_OER/screenshot9.png){width="2.1646347331583553in"
height="1.910336832895888in"}

Im nächsten Dialogfenster können Sie angeben, welchen Bildschirm Sie
aufnehmen möchten Dies ist nur interessant, wenn Sie mit mehreren
angeschlossenen Bildschirmen arbeiten. Bei nur einem Bildschirm ist *0*
die richtige Wahl.

Setzen Sie bei Mauszeiger anzeigen einen Hacken, wenn Sie möchten, dass
der Mauszeiger im späteren Video sichtbar ist. Dies ist zum Beispiel
hilfreich, wenn Sie in Ihren Folien auf konkrete Inhalte zeigen möchten.

Außerdem ist es möglich den Aufnahmebereich zuzuschneiden, um nur einen
Bildausschnitt zu zeigen. Dies ist allerdings meistens nicht notwendig,
da die Präsentation sowieso im Vollbild läuft.

Wundern Sie sich hier nicht über die starke Rückkopplung (endlos
scheinender Tunnel) des Videos, dies geschieht dadurch, dass OBS direkt
anzeigt, was aufgenommen werden soll und da Sie sich im OBS Fenster
befinden, wird die Ausgabe immer wieder abgefilmt und ausgegeben.

Bestätigen Sie mit *Okay*. \[Abb. 15\]

![Einstellungen Bildschirmaufnahme (Screenshot Programmfenster)](images/Anhang_Leitfaden_OBS_OER/screenshot10.png){width="3.2865857392825895in"
height="2.7297583114610675in"}

Nun sollte die aktuelle Bildschirmaufnahme im Vorschaubereich von OBS
(mittlerer Teil des Programmfensters) angezeigt werden. In diesem
Bereich wird alles so angezeigt, wie es später in der Aufnahme zu sehen
sein wird und Sie können die Anordnung und Fenstergröße ihrer Quellen
auf der Leinwand kontrollieren und anpassen.

Fügen Sie im nächsten Schritt ebenso wie die Bildschirmaufnahme, eine
Kameraaufnahme hinzu. Wählen Sie dazu in der bereits bekannten
Quellenliste den Eintrag *Videoaufnahmegerät* aus und benennen diese
Quelle entsprechend.\[Abb. 16\]

![Neue Quelle „Videoaufnahmegerät" hinzufügen (Screenshot Programmfenster)](images/Anhang_Leitfaden_OBS_OER/screenshot11.png){width="1.5716863517060367in"
height="1.7444444444444445in"}

Im auf die Benennung folgenden Dialogfenster können Sie nun neben dem
Punkt *Gerät* Ihre Kamera, mit der Sie sich filmen möchten, aus der
Liste auswählen. Nach der Auswahl sollte Ihre Kamera eingeschaltet
werden und die aktuelle Aufnahme im selben Dialogfenster angezeigt
werden. Sollte dies nicht der Fall sein, kontrollieren Sie bitte, ob
Ihre Kamera richtig angeschlossen und eingestellt ist. Die
voreingestellte Auflösung können Sie übernehmen. Bestätigen Sie Ihre
Auswahl mit *Okay.* \[Abb. 17\]

![Kamera einstellen (Screenshot Programmfenster)](images/Anhang_Leitfaden_OBS_OER/screenshot12.png){width="2.5281692913385827in"
height="2.1321850393700785in"}

Nun sollte Ihre Kameraaufnahme vor der Bildschirmaufnahme angezeigt
werden. Achten Sie darauf, dass sich die Kameraquelle in der
Quellenliste oberhalb der Bildschirmaufnahme befindet. Die Anordnung der
Quellen funktioniert wie ein Stapel Papier: Je weiter unten eine Quelle
in der Liste ist, desto weiter im Hintergrund wird sie angezeigt und
kann somit von den darüber liegenden Quellen überdeckt werden. Die
Reihenfolge lässt sich durch verschieben der Quelleneinträge (Anklicken
und Ziehen) oder mit Hilfe der Pfeile am unteren Rand verändern. \[Abb.
18\]

![Reihenfolge der Quellen verändern (Screenshot Programmfenster)](images/Anhang_Leitfaden_OBS_OER/screenshot13.png){width="2.661971784776903in"
height="2.0281692913385827in"}

Die Größe des Rechtecks, in dem die Kameraaufnahme angezeigt wird, lässt
sich im Vorschaubereich von OBS verändern. Klicken Sie dazu das Video
der Kamera an und ziehen es über eine Ecke der roten Umrandung größer
oder kleiner. Außerdem lässt sich hier das Video in eine beliebige Ecke
schieben. Achten Sie dabei darauf, dass später keine wichtigen Inhalte
verdeckt werden.

#### Aufnahme

Nachdem Sie alles eingestellt haben und ihre Aufnahme arrangiert haben,
können Sie die Aufnahme nun starten. Dazu klicken Sie rechts unten im
Programm auf *Aufnahme starten*. \[Abb. 19\] Nach diesem Klick wird
alles so aufgenommen, wie sie es zuvor eingerichtet haben und
automatisch an dem von ihnen ausgewählten Speicherort abgelegt.

![Aufnahme starten (Screenshot Programmfenster)](images/Anhang_Leitfaden_OBS_OER/screenshot14.png){width="2.377952755905512in"
height="1.374015748031496in"}

Wenn Sie die Aufnahme beenden möchte, klicken Sie auf den nur während
der Aufnahme angezeigten Button *Aufnahme stoppen*. \[Abb. 20\]

Im Unterschied dazu haben Sie über den Pause Knopf daneben, jederzeit
die Möglichkeit eine Aufnahme die pausieren bzw. fortzusetzen.

![Aufnahme stoppen (Screenshot Programmfenster)](images/Anhang_Leitfaden_OBS_OER/screenshot15.png){width="2.6402438757655293in"
height="1.3741994750656168in"}

Bevor Sie mit der Aufnahme Ihres Vortrages beginnen, fertigen Sie bitte
unbedingt eine Testaufnahme an, um zu schauen ob alles richtig
eingestellt ist. Auf folgende Dinge sollte Sie achten:

-   Sind alle wichtigen Bestandteile des Bildschirms zu erkennen und
    nichts abgeschnitten oder überdeckt?

-   Ist die Tonaufnahme des Mikrofons angenehm zu verstehen?

-   Falls Medien mit Ton in der Präsentation abgespielt werden, wird
    dieser Ton aufgenommen?

#### Versenden

Nach dem Sie die Aufnahme erfolgreich beendet haben, müssen Sie diese
nur noch hochladen.

Öffnen Sie dazu den Ordner im Cloudspeicher der TU Dresden, den wir für
Sie angelegt haben (den Link haben wir Ihnen per Mail geschickt). Dort
können Sie nun ihre fertige Aufnahme bequem über den Browser hochladen.
Die Aufnahme finden Sie im Ordner den Sie bei der Einrichtung als
Ausgabeordner für Aufnahmen angeben haben. Sollten Sie den Speicherort
vergessen haben, können Sie den Ordner in OBS auch über den Reiter
*Datei* und dann *Aufnahmen anzeigen* öffnen.

Ihre Aufnahme wird von OBS standardmäßig mit dem Datum und der Uhrzeit
der Aufnahme benannt und trägt die Dateiendung *mkv*.

Im nächsten Schritt werden wir dann die Aufnahme vorne und hinten so
kürzen, dass OBS nicht mehr zu sehen ist und die fertige Datei den
Studierenden über die TU interne Plattform Opal zur Verfügung stellen.
Nur in die URV eingeschriebene Studierende können aktuell Ihre
Aufzeichnung sehen. Allerdings überlegen wird in Zukunft die Videos auch
öffentlich zu teilen, damit so viele Menschen wie möglich Zugang zu
diesen Inhalten haben. Wenn Sie dem Zustimmen, kreuzen Sie das bitte auf
dem beigelegten Formular an.

### Lizensierung - Open Educational Resources (OER)

Bildung sollte unserer Meinung nach allen Menschen frei und kostenlos
zugänglich sein. Um dies zu erreichen, ist das Festhalten und
öffentliche Teilen von spannenden Themen unter offener Lizenz ein großer
Schritt in die richtige Richtung. OER (Open Educational Resources) ist
dabei ein weit verbreiteter Ansatz, dieses Ziel umzusetzen und wir
möchten uns daran beteiligen.

Prinzipiell lassen verschiedene Lizenzen die Verwendung von Materialien
als OER zu.

Wir empfehlen Ihnen die Verwendung der Creative Commons Lizenz [CC BY-SA
4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.de).

Mit dieser Lizenz haben alle Interessierten die Möglichkeit, sich diese
Videos herunter zu laden und in ihren eigenen Vorträgen und Projekten,
unter Angabe des Urhebers, zu verwenden. Das Teilen von Wissen wird
somit ungemein erleichtert!

Letztendlich liegt die Entscheidung jedoch bei Ihnen, wie Sie Ihren
Vortrag und alle damit verbundenen Materialien lizenzieren möchten (Mehr
zu den unterschiedlichen Creative Commons Lizenzen im [Wikipedia
Artikel](https://de.wikipedia.org/wiki/Creative_Commons#Die_aktuellen_Lizenzen))

Wir freuen uns riesig darüber, wenn Sie uns bei diesem Vorhaben helfen
und Ihre Folien und den Vortrag, als einen Beitrag zum Freien Wissen für
alle als OER anbieten und dementsprechend z.B. unter CC BY-SA 4.0 Lizenz
stellen.

Um zu überprüfen, ob Ihr Vortrag und vor allem Ihre Vortragsfolien für
OER geeignet sind bzw. wie Sie diesen entsprechend anpassen können,
entnehmen Sie dem folgenden Leitfaden \[Abb. 21\].

![Flowchart: Leitfaden OER](images/Anhang_Leitfaden_OBS_OER/leitfaden_oer.png){width="2.8943667979002625in"
height="3.4509776902887137in"} 

Wenn Sie Ihren Vortrag als OER zu Verfügung stellen möchten, bestätigen
Sie uns das bitte in der angehängten Einverständniserklärung.

Bei Fragen, Unsicherheiten oder technischen Schwierigkeiten kontaktieren
Sie gerne die URV Verantwortlichen. Diese können entweder direkt
unterstützen oder leiten Ihre Frage ans Technik Team weiter.
