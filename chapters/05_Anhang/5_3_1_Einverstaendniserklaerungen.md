
# 5.3 Vorlagen
## 5.3.1 Einverständniserklärungen
### Einverständniserklärung zur Veröffentlichung auf OPAL und VCS
Hiermit erkläre ich, ________________, mich damit einverstanden, dass die Aufzeichnung meines Vortrages, welchen ich am _____________ im Rahmen der Umweltringvorlesung an der TU Dresden gehalten habe, bis auf Widerruf, auf der Lernplattform OPAL sowie der Plattform Videocampus Sachsen (VCS) begrenzt zugänglich gemacht werden darf.
<!-- Platzhalterfelder für Unterschrift und Datum -->
\medskip

\hrulefill \hfill \hrulefill
Datum\hfill  Unterschrift

### Einwillungserklärung zur Veröffentlichung der Videoaufnahme und der dazugehörigen Dokumente des Beitrags zur Umweltringvorlesung (Vorraussetzung für OER)

Ich sichere zu, alleinige:r Inhaber:in aller Rechte am vorliegenden Werk zu sein. Ich versichere, dass durch die Publikation nicht die Rechte Dritter oder das Gesetz verletzt werden. Dies schließt insbesondere die im Vortrag enthaltenen Abbildungen (Fotos, Grafikelemente etc.) ein. Ich verpflichte mich, den Betreiber (TU-Umweltinitiative Dresden (tuuwi)) unverzüglich zu informieren, wenn Dritte Ansprüche auf Grund der ihnen zustehenden Urheber- bzw. Nutzungsrechte in Bezug auf das bezeichnete Dokument geltend machen. Damit wird der Betreiber von allen Ansprüchen, die Dritte auf Grund ihnen zustehender Urheber- bzw. Nutzungsrechte in Ansehung des Dokuments gegen den Betreiber erheben, freigestellt. Des Weiteren werden dem Betreiber die aus der Inanspruchnahme entstehenden Kosten erstattet. Daher verpflichte ich mich, dem Betreiber auf dessen Anfrage jede Einräumung von Nutzungsrechten an diesem Werk an Dritte sowie deren Umfang mitzuteilen.

Darüber hinaus übertrage ich hiermit der tuuwi die Herausgeberschaft meiner Aufzeichnung und demnach alle Rechte zur nicht-kommerziellen Nutzung. 

Außerdem erhält die tuuwi das Recht, meine Inhalte zu bearbeiten und Unterlizenzen zu vergeben. Diese Rechte sind in jeglicher Form zeitlich und räumlich unbegrenzt sowie exklusiv. Dies ist notwendig, damit die tuuwi das Werk unter CC BY-SA Lizenz für mich veröffentlichen kann. 

<!-- Platzhalterfelder für Unterschrift und Datum -->
\medskip

\rule{5cm}{0.2mm} \hfill \rule{5cm}{0.2mm}
Titel des Vortrages\hfill Datum des Vortrages
\medskip

\rule{5cm}{0.2mm}
Name,Vorname
\medskip

\rule{5cm}{0.2mm} \hfill \rule{5cm}{0.2mm}
Datum\hfill  Unterschrift

\newpage{}