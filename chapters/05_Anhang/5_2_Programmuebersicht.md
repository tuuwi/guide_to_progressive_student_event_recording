
# 5.2 Programmübersicht

In diesem Abschnitt findet sich eine Liste der Tools, die wir im Laufe
unserer Arbeit verwendet haben. \
Sofern nicht anders gekennzeichnet, handelt es sich hierbei um freie
Software bzw. Open-Source-Software.

## Verwaltung / Teilen von Dateien / Dokumenten

-   **Humhub**

    -   umfangreiche Plattform zur Zusammenarbeit in Gruppen

    -   beschreibt sich selbst als \"Social Network Kit\", da es vom
        Aufbau und der Bedienung mit Widgets und Timelines an ein
        soziales Netwerk erinnert

    -   bietet an Funktionen u. a. gemeinsames Wiki, Dateiverwaltung,
        Kalender und kann über Plugins stark erweitert werden

    -   bildet das Herzstück der digitalen Zusammenarbeit der tuuwi, in
        dem wir dort Termine eintragen, Bilder und Protokolle
        archvieren, Dateien verwalten

    -   im DF-Projekt vor allem für die Wissenspeicherung, Dokumentierung
        und Aufgabenverwaltung verwendet 
    - offizielle Webseite: [humhub.com](https://www.humhub.com/de)

-   **Nextcloud**

    -   Software zur Erstellung von Cloudspeichern

    -   für Studierende/Mitarbeitende/Hochschulgruppen bietet die TU
        Dresden eine eigene Instanz unter cloudstore.zih.tu-dresden.de
        an

    -   als tuuwi haben wir dort einen eigenen Cloudspeicher bekommen,
        wie wir diesen verwendet haben, wird im Kapitel \"Kommunikation
        mit den Referierenden\" beschrieben 
    -   offizielle Webseite: [nextcloud.com](https://nextcloud.com/)

-   **Etherpad**

    -   Software für das schnelle und einfache kollaborative Schreiben
        von Dokumenten direkt im Browser

    -   als tuuwi betreiben wir eine eigene Instanz

    -   Vorstellung/Anleitung: ["Mit Pads gemeinsam online an Texten schreiben"](https://tuuwi.de/2020/04/04/einfach-gemeinsam-an-texten-arbeiten-pads-machen-es-moeglich/) (Blogartikel auf tuuwi.de)

    -   offizielle Webseite: [etherpad.org]((https://etherpad.org/))


-   **OnlyOffice**

    -   Möglichkeit Office Dokumente direkt im Browser zu bearbeiten 

    -   z\. B. zur Erstellung des Leitfadens genutzt

    -   nützlich, wenn ein festes Layout und mehr Bearbeitungstools als
        in Etherpad Lite benötigt werden

    -   der StuRa der TU Dresden stellt eine eigene Instanz bereit, die
        wir verwendet haben
    
    -   offizielle Webseite: [onlyoffice.com](https://www.onlyoffice.com/)

-   **Lufi (ähnlich zu Firefox Send)**

    -   Browsertool zum einfachen Teilen von verschlüsselten Dokumenten

    -   vor allem nützlich, wenn Dokumente übersendet werden, die
        persönliche Informationen enthalten 

    -   Dokumente werden direkt im Browser verschlüsselt und nur
        verschlüsselt auf dem Server gespeichert

    -   Es lässt sich unter anderem eine Ablaufzeit einstellen, zu der
        die Dokumente nicht mehr einsehbar sind

    -   Am Ende hat man einen Downloadlink, den man einfach teilen kann 

    -   Mögliche Instanz, die verwendet werden
        kann: [upload.disroot.org](https://upload.disroot.org/)

    -   kann mit wenig Aufwand selber gehostet werden
    
    -   Source-Code: [framagit.org/fiat-tux/hat-softwares/lufi](https://framagit.org/fiat-tux/hat-softwares/lufi)

## Videoaufzeichnung/-bearbeitung

-   **OBS (Open Broadcaster Software)**

    -   umfangreiche Aufzeichnungssoftware, die das Aufzeichnen des
        Bildschirms und Tons ermöglicht

    -   hauptsächlich für die Asynchrone URV in den Coronasemestern
        verwendet

    -   weitere Informationen im OBS-Leitfaden (Anh. 5.3.2)

    -   offizielle Webseite: [obsprojekt.com](https://obsproject.com/)

-   **Shotcut**

    -   einfaches Videoschnittprogramm

    -   Aufgrund der Aufzeichnung mit OBS in den Coronasemestern, welche
        bereits viele Möglichkeiten, wie z.B. das Erstellen einer Szene
        mitbringt, benötigten wir erstmal keine Schnittsoftware 

    -   sobald wieder Präsenzvorlesungen mit Aufnahme möglich sind, wird
        aber definitiv ein Schnittprogramm benötigt, um z.B. die Video-
        und Tonspuren zusammenzuführen

    -   offizielle Webseite: [shotcut.org](https://shotcut.org/)

-   **Handbrake**

    -   Software zur Komprimierung von Videos, d.h. um sie vor dem
        Upload zu verkleinern 

    -   sehr einfache Handhabung und erzeugt ohne große Einstellung
        schon deutlich kleinere Videos, ohne merkbaren Qualitätsverlust
    
    -   offizielle Webseite: [handbrake.fr](https://handbrake.fr/)

## Veröffentlichung

-   **OPAL**

    -   Lernplattform des Bildungsportals Sachsen

    -   verwendet für Onlineeinschreibung in die URV, Kontakt zu den
        eingeschriebenen Studierenden und im digitalen Semester zur
        Veröffentlichung der Vorlesungsvideos in geschlossenem Rahmen

    -  **proprietär / Quellcode nicht offen**

    - offizielle Webseite: [bildungsportal.sachsen.de/opal](https://bildungsportal.sachsen.de/opal/)

-   **Videocampus Sachsen**

    -   Videoportal des Bildungsportals Sachsen

    -   ermöglicht das überwiegend hochschulinterne Teilen von Videos 

    -   ist an OPAL angebunden

    -   **proprietär / Quellcode nicht offen**
    
    -   offizielle Webseite: [videocampus.sachsen.de](https://videocampus.sachsen.de/)

-   **Peertube**

    -   dezentrale Videoplattform

    -   freie Alternative zu Youtube ohne Werbung

    -   kann mittlerweile auch Livestreaming

    -   entwickelt durch die Non-Profit-Organsiation Framasoft

    -   offizielle Webseite: [joinpeertube.org](https://joinpeertube.org/) 

## Kommunikation

-   **Telegram**

    -   für die interne Kommunikation des Projektteams haben wir,
        aufgrund der starken Etablierung des Messengers innerhalb der
        tuuwi, Telegram verwendet

    -   Features, wie Terminabstimmungen, Fileupload,
        Sprach/Videonachrichten haben uns die Kommunikation sehr
        erleichtert

    -   aus Datenschutz und Sicherheitssicht ist Telegram allerdings
        weniger zu empfehlen, da unter anderem nicht gerade datensparsam und keine Ende zu Ende Verschlüsselung von Gruppenchats: [\"Telegram: \'Sicherheit\' gibt es nur auf Anfrage\"](https://www.kuketz-blog.de/telegram-sicherheit-gibt-es-nur-auf-anfrage-messenger-teil3/) (Blogartikel von Mike Kuketz)

        -    bessere Alternativen wären z.B. Signal, Threema oder Matrix

    -   nur die Clientanwendungen (App, Webanwendung und
        Desktopanwendung) von Telegram sind OpenSource, der Servercode
        ist nicht einsehbar

    -   offizielle Webseite: [telegram.org](https://telegram.org/)

-   **Email**

    -   für den Kontakt zu Referierenden, offiziellen Stellen und der
        Verwaltung ist weiterhin Email das Mittel der Wahl

    -   für die Kommunikation per E-Mail haben wir meist unsere
        offiziellen E-Mail-Adressen der TU Dresden verwendet

-   **weitere Tools**

    -   [dudle](https://dudle.inf.tu-dresden.de/)
        (Terminabstimmungstool der TU-Dresden)

    -   [Matrix](https://doc.matrix.tu-dresden.de) (umfassendes
        Chat-Tool)

## Konferenzsoftware
-   **BigBlueButton (BBB)**

    -   Konferenzsoftware

    -   ermöglicht einfache digitale Treffen 

    -   sowohl zur Teambesprechung als auch für Livesessions der URV
        genutzt

    -   größerer Funktionsumfang

    -   im Laufe der Pandemie ebenfalls von der TU bereitgestellt, lange
        Zeit haben wir aber auch eine eigene Instanz verwendet
        
    -   umfangreiches Praxishandbuch: [soethe.net/bigbluebutton](https://soethe.net/bigbluebutton/)
    -   offizielle Webseite: [bigbluebutton.org](https://bigbluebutton.org/)

-   **GoToMeeting**

    -   da im zweiten Coronasemester sehr viele Menschen an den URV
        teilgenommen haben und wir wieder ein Liveformat an bieten
        wollten, mussten wir dafür leider auf diese beiden proprietären
        Anwendungen ausweichen =\> für über 200 Menschen in einem Raum,
        haben die uns zur Verfügung stehenden BBB Instanzen leider nicht
        ausgereicht

    -   **proprietär und Serverstandort in den USA**

    -   wenn immer möglich, empfehlen wir die Nutzung von freien
        Alternativen wie z.B. BBB

\newpage{}

## Didaktik
*\...um ein bisschen die Online-Lehre aufzupeppen:*

**Leider sind alle hier gennanten Didaktik-Tools proprietär bzw. wir konnten keinen aktuellen Quellcode finden.**

-   [oncoo.de](https://www.oncoo.de/)

    -   Oncoo ermöglicht unter anderem Kartenabfragen,
        Zielscheibenfeedback usw.

-   [tricider.com](https://www.tricider.com)

    -   Tricider ermöglicht es u.a., Fragen zu stellen und dafür
        Argumente und Vorschläge zu sammeln.

-   [answergarden.ch](https://answergarden.ch/)

    -   Wortwolken, Assoziationen sammeln

-   [tweedback.de](https://tweedback.de/)

    -   Live-Feedback

Weiterhin könnt ihr natürlich die in den Videochatsoftwares vorhandenen
Funktionen nutzen. Wie bspw. in BBB die Umfragefunktion, das Whiteboard
in Mehrbenutzermodus usw.

Mehr Tools findest du u.a. hier:

-   [Kollaborative Materialsammlung](https://hackmd.io/@m3lgTmR3Q-ugT7xywwUsJw/HyU0s0dM7#Kollaborative-Materialsammlung)

-   [Umfangreiche Toolübersicht mit Tool-o-search](https://wbdig.guetesiegelverbund.de/tools)
